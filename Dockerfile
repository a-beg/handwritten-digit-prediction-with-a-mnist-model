FROM python:3.8-slim

# Selecting working directory
WORKDIR /src/prediction_app

# Copying needed files
COPY requirements.txt .
COPY prediction_script.py .
COPY mnist-8.onnx .

# Installing dependencies and python libraries
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    apt-utils \
    libglib2.0-0 \
    libgl1-mesa-dev \
    python3 \
    python3-pip \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    pip install -r requirements.txt

# Entrypoint when running the dockerImage
ENTRYPOINT ["python3", "prediction_script.py"]

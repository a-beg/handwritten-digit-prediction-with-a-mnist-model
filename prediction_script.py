# This script predicts a handwritten digit image with the prediction model MNIST
# To use this script without going through the docker container replace line 30 with image = cv2.imread(arguments[i]) and use the
# command "python3 prediction_script.py imageName.png"
# Note that you can pass multiple images at the same time
# The output is a JSON file named data.txt, generated in the current folder, where you can find the predictions and their percentages

def main(arguments):
        
    import onnxruntime as rt
    import numpy
    import numpy as np
    import cv2
    import json
    
    # data is a dictionnary where predictions and their percentages will be stored 
    data = {}

    if len(arguments) <= 1:
        print("Please give at least one image name as argument")
    else :
        # Loading the pretrained model and getting its input and output names
        sess = rt.InferenceSession("mnist-8.onnx")
        input_name = sess.get_inputs()[0].name
        output_name = sess.get_outputs()[0].name
        
    for i in range (1,len(arguments)):
        
        # Reading images
        print("Reading image",i)
        image = cv2.imread("data/"+arguments[i])
            
            
        if (image is not None):
                
            # Image reshaping to the shape accepted by the model 
            print("Reshaping")
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            gray = cv2.resize(gray, (28,28)).astype(np.float32)/255
            input_im = np.reshape(gray, (1,1,28,28))
            
            # Running the predictions ( the output of this function is a list of 10 numbers, the index of the highest number represents the most probable prediction )
            print("Running prediction")
            res = sess.run([output_name], {input_name: input_im})
                
            # Getting the prediction and its percentage 
            number_predicted = ((res[0][0]).tolist()).index(max(res[0][0]))
            percentage = max(100*np.exp(res[0][0])/sum(np.exp(res[0][0]))) # the function softmax is applied here to get a percentage instead of an <insignificant> number
                
            # Adding the prediction and its percentage to the data dictionnary
            data[arguments[i]] = []
            data[arguments[i]].append({
                'Number predicted': number_predicted,
                'Percentage': str(percentage)
            })
                
        else :
            print("Not able to read image number",i,"\n")
                
        # Converting data into a JSON file and printing it
    print("Saving results into a JSON file")
    with open('data/data.txt', 'w') as outfile:
        jstr = json.dump(data, outfile, ensure_ascii=False, indent=4)
        jstr = json.dumps(data, ensure_ascii=False, indent=4)
        print("JSON file containing results saved as data.txt in the current folder")
        print(jstr)
        
if __name__ == '__main__':
    import sys
    main(sys.argv)

# Handwritten-digit-prediction-with-a-MNIST-model
This is a python application, containerized with docker, used to predict handwritten-digits with a MNIST model.
## Table of contents
* [General info](#general-info)
* [Docker engine](#docker-engine)
* [Setup the application](#setup-the-application)
* [Testing the application](#testing-the-application)

## General info
This project shows how to simply install an application in a Unix environment to predict handwritten-digits with a MNIST onnx model which uses a convolutional neural network (CNN). We used in this project the [mnist](https://github.com/onnx/models/tree/master/vision/classification/mnist) with **ONNX version 1.3** and **Opset version 8**.

In order to install this application we first need to install Docker. 
## Docker engine
### What is docker and what is the purpose of using it
Docker is used to simplify the process of building an application using containers. A container allows us to package up all the parts we need for our application such as libraries and any other type of dependencies. By doing so, we can be sure that our application will run in any other Linux machine regardless of any settings' differences between the target machines and the machine where the application was built and deployed.
### Installing docker engine
In order to install docker engine you can visit [docker install](https://docs.docker.com/engine/install/) where you can choose your linux distribution and follow the instructions.

## Setup the application
First, download and extract the zip file. Then, go inside the folder and type the following command :
```
$ sudo docker build --rm -t number_prediction .
```
This command is used to build the docker image (the application). Command explaining :
* **sudo** allows you to run programs with superuser privileges
* **docker build** is used to build the docker image
* **--rm** flag is to remove the docker container if it exists
* **-t** flag is used to tag the image so that we can refer to it afterwards with other docker commands
* **number_prediction** is name of your image, you can name it whatever you want.
This command takes a while, because it has to download all the application dependencies.

When this command is done, the application is installed and ready to use.

## Testing the application
In this project there is a testing image which represents the digit 6 named **six_test_image.png**. To test our application with this image, run the following command in the folder containing **six_test_image.png** :
```
$ sudo docker run -v "$(pwd)":/src/prediction_app/data number_prediction six_test_image.png
```
When you execute this command, a file named data.txt is created in the current folder. This file is in JSON format and contains the predictions. It should look like this :

![Screenshot_from_2021-04-05_00-40-16](/uploads/dfec1f78a5980359a1e7445578f5bce4/Screenshot_from_2021-04-05_00-40-16.png)

Note that this command can be run from anywhere in your machine. Only the image used has to be in the folder where you are running this command from.

You can also predict the digits for multiple images at the same time, see bellow :

![Screenshot_from_2021-04-05_00-56-53](/uploads/60bf886bf208bf85136c019055fb571f/Screenshot_from_2021-04-05_00-56-53.png)
